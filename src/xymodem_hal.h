/** @file xymodem_hal.h
 *
 * @brief Hardware Abstraction Layer function definitions and 
 * @brief and stucts
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Ashton Johnson. See xymodem/LICENSE
 */
#ifndef XYMODEM_HAL_H
#define XYMODEM_HAL_H

#include <stdint.h>

#define XYM_OKAY (0)
#define XYM_INIT_FAIL (-1)

/**
 * Initialize the serial device
 *
 * @param HAL specific struct for initializing the serial device
 * @return XYM_OKAY if initialization was sucessful
 */
int xymodem_serial_init(void);


#define XYMODEM_RX_TIMEOUT (-1)
#define XYMODEM_NO_TIEMOUT (0u)
/**
 * Receive the specified number of bytes from the serial device
 * @param The receive buffer pointer to store received bytes
 * @param The number of bytes to receive 
 * @param The timeout in miliseconds between each byte received before returning
 * @return The number of bytes received before the a timeout occured.
 */
int xymodem_serial_rx(const uint8_t * p_rx_buffer, const uint_least16_t num_of_bytes, const unsigned int timeout_ms);

/**
 * Transmit the specified number of bytes via the serial device.
 * @param The receive buffer pointer to store received bytes
 * @param The number of bytes to receive 
 * @param The timeout in miliseconds between each byte received before returning
 * @return XYM_OKAY if the requests bytes were sucessfully transmitted 
 */ 
int xymodem_serial_tx(const uint8_t * p_tx_buffer, const uint_least16_t num_of_bytes, const unsigned int timeout_ms);


#endif /* XYMODEM_HAL_H */
/*** end of file ***/
