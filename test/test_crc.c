/**********************************************************************
 *
 * Filename:    test_crc.c
 * 
 * Description: A simple test program for the CRC implementations.
 *
 * Notes:       To test a different CRC standard, modify crc.h.
 *
 * Revisions : 1.0 [MB] Initial Release
 *           : 2.0 [AJ] Converted to unit test case
 *  
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 * Copyrigth (c) 2018 Ashton Johnson. See xymodem/LICENSE.
 **********************************************************************/

#include <stdio.h>
#include <string.h>

#include "unity.h"
#include "crc.h"

void setUp(void)
{
 crcInit();
}


void testDown(void)
{
  
}

void
test_crc_check_value(void)
{
	unsigned char  test[]= "123456789";
	int crc_result;

	printf("Chosen CRC = %s\r\n", CRC_NAME);
	printf("CHECK_VALUE = 0x%X\r\n", CHECK_VALUE);	
	
	/*
	 * Compute the CRC of the test message, slowly.
	 */
	crc_result = crcSlow(test, strlen(test));
	TEST_ASSERT_EQUAL_MESSAGE(CHECK_VALUE, crc_result, "crcSlow() failed");
	/*
	 * Compute the CRC of the test message, more efficiently.
	 */	

	crc_result = crcFast(test, strlen(test));
	TEST_ASSERT_EQUAL_MESSAGE(CHECK_VALUE, crc_result, "crcFast() failed");
} 

void
test_crc_ascii_zeros(void)
{
	unsigned char  test[9] = "000000000";
	const uint16_t chk_value = 0x12bb; // from crccalc.com CRC-16/CCITT-FALSE
	int crc_result;

	/*
	 * Compute the CRC of the test message, slowly.
	 */
	crc_result = crcSlow(test, strlen(test));
	TEST_ASSERT_EQUAL_MESSAGE(chk_value, crc_result, "crcSlow() failed");
	/*
	 * Compute the CRC of the test message, more efficiently.
	 */	
	crcInit();
	crc_result = crcFast(test, strlen(test));
	TEST_ASSERT_EQUAL_MESSAGE(chk_value, crc_result, "crcFast() failed");
} 


