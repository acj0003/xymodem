/**********************************************************************
 *
 * Filename:    test_xilinx_zynq_ps.c
 * 
 * Description: A simple test program for the CRC implementations.
 *
 * Notes:       To test a different CRC standard, modify crc.h.
 *
 * Revisions : 1.0 [MB] Initial Release
 *           : 2.0 [AJ] Converted to unit test case
 *  
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 * Copyrigth (c) 2018 Ashton Johnson. See xymodem/LICENSE.
 **********************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "unity.h"
#include "xymodem_hal.h"
#include "mock_xuartps.h"

void setUp(void)
{

}


void tearDown(void)
{
  
}

void test_xymodem_serial_init_nom(void){
  XUartPs ser;
  XUartPs_Config cfg; 
  XUartPs_LookupConfig_IgnoreAndReturn(&cfg);
  XUartPs_CfgInitialize_IgnoreAndReturn(XST_SUCCESS);
  XUartPs_SelfTest_IgnoreAndReturn(XST_SUCCESS);
  XUartPs_SetBaudRate_IgnoreAndReturn(XST_SUCCESS);
  XUartPs_SetOperMode_Ignore();

  
  int init_result = xymodem_serial_init();
  TEST_ASSERT_EQUAL(init_result, XYM_OKAY);

}


void test_xymodem_serial_rx_sixteen_bytes_no_timeout(void){

  const unsigned int timeout_ms = 0;
  u8 buffer[16]= {0, 15, 1, 14,
		  2, 13, 3, 12,
		  4, 11, 5, 10,
		  6, 9, 7, 8};
  XUartPs ser;
  

  for( uint_least16_t num_of_bytes = 1; num_of_bytes <= 16; num_of_bytes++){ 
    /* Set Up Mocks */
    XUartPs_Recv_ExpectWithArrayAndReturn(&ser, 0, buffer, num_of_bytes, num_of_bytes, num_of_bytes);
    XUartPs_Recv_IgnoreArg_InstancePtr();
    
    /* Test the module */
    int result = xymodem_serial_rx(buffer, num_of_bytes, timeout_ms);

    /* Confirm Results */
    TEST_ASSERT_EQUAL_UINT16(num_of_bytes, result);

  }
}

void test_xymodem_serial_rx_retry_no_timeout(void){
  const unsigned int timeout_ms = 0;
  u8 buffer[16]= {0, 15, 1, 14,
		  2, 13, 3, 12,
		  4, 11, 5, 10,
		  6, 9, 7, 8};
  XUartPs ser;


  for( uint_least16_t num_of_bytes = 16; num_of_bytes <= 16; num_of_bytes++){ 
    /* Set Up Mocks */
    XUartPs_Recv_ExpectWithArrayAndReturn(&ser, 0, buffer, num_of_bytes, num_of_bytes, 7);
    XUartPs_Recv_IgnoreArg_InstancePtr();


     XUartPs_Recv_ExpectWithArrayAndReturn(&ser, 0, buffer, num_of_bytes, num_of_bytes-7,9);
     XUartPs_Recv_IgnoreArg_InstancePtr();
    /* Test the module */
    int result = xymodem_serial_rx(buffer, num_of_bytes, timeout_ms);

    /* Confirm Results */
    TEST_ASSERT_EQUAL_UINT16(num_of_bytes, result);

  }
}



void test_xymodem_serial_tx_sixteen_byte_no_timeout(void){
  const unsigned int timeout_ms = 0;
  u8 buffer[16]= {0, 15, 1, 14,
		  2, 13, 3, 12,
		  4, 11, 5, 10,
		  6, 9, 7, 8};
  XUartPs ser;


  for( uint_least16_t num_of_bytes = 1; num_of_bytes <= 16; num_of_bytes++){ 
    /* Set Up Mocks */
    XUartPs_Send_ExpectWithArrayAndReturn(&ser, 0, buffer, num_of_bytes, num_of_bytes, num_of_bytes);
    XUartPs_Send_IgnoreArg_InstancePtr();
    
    /* Test the module */
    int result = xymodem_serial_tx(buffer, num_of_bytes, timeout_ms);

    /* Confirm Results */
    TEST_ASSERT_EQUAL(result, XYM_OKAY);

  }
}


void test_xymodem_serial_tx_retry_no_timeout(void){
  const unsigned int timeout_ms = 0;
  u8 buffer[16]= {0, 15, 1, 14,
		  2, 13, 3, 12,
		  4, 11, 5, 10,
		  6, 9, 7, 8};
  XUartPs ser;


  for( uint_least16_t num_of_bytes = 16; num_of_bytes <= 16; num_of_bytes++){ 
    /* Set Up Mocks */
    XUartPs_Send_ExpectWithArrayAndReturn(&ser, 0, buffer, num_of_bytes, num_of_bytes, 7);
    XUartPs_Send_IgnoreArg_InstancePtr();


     XUartPs_Send_ExpectWithArrayAndReturn(&ser, 0, buffer, num_of_bytes, num_of_bytes-7,9);
     XUartPs_Send_IgnoreArg_InstancePtr();
    /* Test the module */
    int result = xymodem_serial_tx(buffer, num_of_bytes, timeout_ms);

    /* Confirm Results */
    TEST_ASSERT_EQUAL(result, XYM_OKAY);

  }
}

