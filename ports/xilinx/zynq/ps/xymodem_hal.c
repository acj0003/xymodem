/** @file xilinx_zynq_ps_hal.h
 *
 * @brief Hardware Abstraction Layer function definitions and 
 * @brief and stucts
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Ashton Johnson. See xymodem/LICENSE
 */


#include "xymodem_hal.h"
#include "xparameters.h"
#include "xuartps.h"
#include "xil_printf.h"



#define UART_DEVICE_ID              XPAR_XUARTPS_1_DEVICE_ID
#define USE_FAT_FS

#ifdef USE_FAT_FS
#include "ff.h"
typedef FATFS xym_fs_t;
typedef FIL xym_file_t;
#elif USE_XIL_MEM_FS
#include "xilmfs.h"
typedef void xym_fs_t; 
typedef int xym_file_t; /* defined by mfs_file_open() */
#else
#error "Memory Filesystem not defined"
#endif


/**
 * struct used by HAL functions. 
 * to be defined in the port specific HAL.
 */
typedef struct {
  XUartPs ser;
  XUartPs_Config * p_ser_config;
  xym_fs_t fs;
  xym_file_t file;
} xymodem_hal_serial_conf_t; 


static xymodem_hal_serial_conf_t xym;

/**
 * Initialize the serial device
 *
 * @param HAL specific struct for initializing the serial device
 * @return XYM_OKAY if initialization was sucessful
 */
int xymodem_serial_init(void){

#ifdef USE_STDIO
  setvbuf(stdin, NULL, _IONBF, 0);
#else
#ifdef PLATFORM_ZYNQ

  /*
   * Initialize the UART driver so that it's ready to use.
   * Look up the configuration in the config table, then initialize it.
   */
  xym.p_ser_config = XUartPs_LookupConfig(UART_DEVICE_ID);
  if(NULL ==xym.p_ser_config){
    return XYM_INIT_FAIL;
  }

  int status = XUartPs_CfgInitialize(&xym.ser,
				     xym.p_ser_config,
				     xym.p_ser_config->BaseAddress);
  if (XST_SUCCESS != status){
    return XYM_INIT_FAIL;
  }


  /* Check hardware build. */
  status = XUartPs_SelfTest(&xym.ser);
  if (XST_SUCCESS != status){
    return XYM_INIT_FAIL;
  }


  status = XUartPs_SetBaudRate(&xym.ser, 115200);
  if (XST_SUCCESS != status){
    return XYM_INIT_FAIL;
  }

  /* Ensure not in loopback*/
  XUartPs_SetOperMode(&xym.ser, XUARTPS_OPER_MODE_NORMAL);

#endif
#endif

  return XYM_OKAY;
}


#define XYMODEM_RX_TIMEOUT (-1)
#define XYMODEM_NO_TIEMOUT (0u)
/**
 * Receive the specified number of bytes from the serial device
 * @param The receive buffer pointer to store received bytes
 * @param The number of bytes to receive 
 * @param The timeout in miliseconds between each byte received before returning
 * @return The number of bytes received before the a timeout occured.
 */
int xymodem_serial_rx(const uint8_t * p_rx_buffer, const uint_least16_t num_of_bytes, const unsigned int timeout_ms){

  u8 recv_per_round = 0; 
  u8 req_to_recv = num_of_bytes; 
  do{
    recv_per_round = XUartPs_Recv(&xym.ser, (u8 *)p_rx_buffer, req_to_recv);
      req_to_recv -= recv_per_round; 
  } while(req_to_recv); 
  return (uint8_t) num_of_bytes; 

}

/**
 * Transmit the specified number of bytes via the serial device.
 * @param The receive buffer pointer to store received bytes
 * @param The number of bytes to receive 
 * @param The timeout in miliseconds between each byte received before returning
 * @return XYM_OKAY if the requests bytes were sucessfully transmitted 
 */ 
int xymodem_serial_tx(const uint8_t * p_tx_buffer, const uint_least16_t num_of_bytes, const unsigned int timeout_ms){

   u8 sent_per_round = 0; 
  u8 req_to_send = num_of_bytes; 
  do{
    sent_per_round = XUartPs_Send(&xym.ser, (u8 *)p_tx_buffer, req_to_send);
      req_to_send -= sent_per_round; 
  } while(req_to_send); 
  
  return  (0 == req_to_send) ? XYM_OKAY : -1; 
}


/*** end of file ***/
